
let sorting = (arr) => {

    
    var len = arr.length;
    for (var i = len - 1; i >= 0; i--) {
        for (var j = 1; j <= i; j++) {
            if (arr[j - 1] > arr[j]) {
                var temp = arr[j - 1];
                arr[j - 1] = arr[j];
                arr[j] = temp;
            }
        }
    }
        
    

    return arr;
}

let compare = (a, b) => {
    if (parseInt(a['PM2.5']) > parseInt(b['PM2.5'])) {
        return 1;
    }
    if (parseInt(a['PM2.5']) < parseInt(b['PM2.5'])) {
        return -1;
    }
    return 0;
}

let average = (nums) => {
    var total = 0;
    for (var i = 0; i < nums.length; i++) {
        total += nums[i];
    }
    var avg = total / nums.length;

    //var avground = (Math.round(avg * 100) / 100).toFixed(2);
    var avground = Math.floor(avg * 100) / 100;

    return avground;


}




module.exports = {
    sorting,
    compare,
    average
}